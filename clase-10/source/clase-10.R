## **[4.] OpenStreetMap**
  
### **4.0 Configuración inicial**
rm(list=ls())

## llamar pacman (contiene la función p_load)
require(pacman)

## llamar y/o instalar librerias
p_load(tidyverse,rio,skimr,
       sf, ## datos espaciales
       leaflet, ## visualizaciones
       tmaptools, ## geocodificar
       ggsn, ## map scale bar 
       osmdata) ## packages with census data

### **4.1 Acerca de OpenStreetMap**

### **4.2. Geocodificar direcciones**
  
## Buscar un lugar público por el nombre
geocode_OSM("Casa de Nariño, Bogotá")

## geocode_OSM no reconoce el caracter #, en su lugar se usa %23% 
cbd <- geocode_OSM("Centro Internacional, Bogotá", as.sf=T) 
cbd

## la función addTiles adiciona la capa de OpenStreetMap
leaflet() %>% addTiles() %>% addCircles(data=cbd)

### **4.3. Librería `osmdata`**

#### **4.3.1. Features disponibles**
available_features() %>% head(100)

available_tags("amenity") %>% head(20)

### **4.4. Descargar features**

## obtener la caja de coordenada que contiene el polígono de Bogotá
opq(bbox = getbb("Bogotá Colombia"))

## objeto osm
osm = opq(bbox = getbb("Bogotá Colombia")) %>%
      add_osm_feature(key="amenity" , value="bus_station") 
class(osm)

## extraer Simple Features Collection
osm_sf = osm %>% osmdata_sf()
osm_sf

## Obtener un objeto sf
bus_station = osm_sf$osm_points %>% select(osm_id,amenity) 
bus_station

## Pintar las estaciones de autobus
leaflet() %>% addTiles() %>% addCircles(data=bus_station , col="red")

## **[5.] Operaciones geometricas**

### **5.1 Importar conjuntos de datos**

## Inmuebles
houses <- import("input/house_prices.rds")
class(houses)
skim(houses)

## dataframe to sf
houses <- st_as_sf(x = houses, ## datos
                   coords=c("lon","lat"), ## coordenadas
                   crs=4326) ## CRS
class(houses)

leaflet() %>% addTiles() %>% addCircleMarkers(data=houses[100000:100010,])

## parques
parques <- opq(bbox = getbb("Bogota Colombia")) %>%
           add_osm_feature(key = "leisure", value = "park") %>%
           osmdata_sf() %>% .$osm_polygons %>% select(osm_id,name)

leaflet() %>% addTiles() %>% addPolygons(data=parques)

### **5.2 help:** `sf` 

## Help
vignette("sf3")
vignette("sf4")

### **5.3 Afine transformations**
parques_2 = st_transform(parques,crs=4322)
st_crs(houses) == st_crs(parques) 
parques_3 = st_transform(parques_2,crs=st_crs(houses))

### **5.4 Filtrar datos**

## usando los valores de una variable
houses1 <- houses %>% subset(l3=="Bogotá D.C") %>% subset(l4=="Zona Chapinero")

leaflet() %>% addTiles() %>% addCircles(data=houses1)

## usando la geometría
chapinero <- getbb(place_name = "UPZ Chapinero, Bogota", 
                   featuretype = "boundary:administrative", 
                   format_out = "sf_polygon") %>% .$multipolygon

leaflet() %>% addTiles() %>% addPolygons(data=chapinero)

## crop puntos con poligono (opcion 2)
house_chapi <- st_intersection(x = houses , y = chapinero)

leaflet() %>% addTiles() %>% addPolygons(data=chapinero,col="red") %>% addCircles(data=house_chapi)

## crop puntos con poligono (opcion 3)
house_chapi <- houses[chapinero,]

leaflet() %>% addTiles() %>% addPolygons(data=chapinero,col="red") %>% addCircles(data=house_chapi)

### **5.5. Distancia a amenities**

## Distancia a un punto
house_chapi$dist_cbd <- st_distance(x=house_chapi , y=cbd)

house_chapi$dist_cbd %>% head()

## Distancia a muchos puntos
matrix_dist_bus <- st_distance(x=house_chapi , y=bus_station)

matrix_dist_bus[1:5,1:5]

min_dist_bus <- apply(matrix_dist_bus , 1 , min)

min_dist_bus %>% head()

house_chapi$dist_buse = min_dist_bus

## Distancia a muchos polygonos
matrix_dist_parque <- st_distance(x=house_chapi , y=parques)

matrix_dist_parque[1:5,1:5]

mean_dist_parque <- apply(matrix_dist_parque , 1 , mean)

mean_dist_parque %>% head()

house_chapi$dist_parque = mean_dist_parque


